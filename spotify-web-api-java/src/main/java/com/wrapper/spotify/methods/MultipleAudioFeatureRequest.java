package com.wrapper.spotify.methods;

import com.google.common.base.Joiner;
import com.google.common.util.concurrent.SettableFuture;
import com.wrapper.spotify.exceptions.WebApiException;
import com.wrapper.spotify.models.AudioFeature;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MultipleAudioFeatureRequest extends AbstractRequest {


    public MultipleAudioFeatureRequest(Builder builder) {
        super(builder);
    }

    public List<AudioFeature> get() throws IOException, WebApiException {
        String jsonString = getJson();
        JSONObject jsonObject = JSONObject.fromObject(jsonString);

        return createMultipleAudioFeature(jsonObject);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends AbstractRequest.Builder<Builder> {

        /**
         * The audio request with the given song id.
         *
         * @param ids The ids for the song.
         * @return AlbumRequest
         */
        public Builder id(List<String> ids) {
            assert (ids != null);
            String idsParameter = Joiner.on(",").join(ids).toString();
            path("/v1/audio-features");
            return parameter("ids", idsParameter);
        }

        public MultipleAudioFeatureRequest build() {
            return new MultipleAudioFeatureRequest(this);
        }

    }

    private static AudioFeature createAudioFeature(JSONObject audioFeatureJson) {
        if (audioFeatureJson == null || audioFeatureJson.isNullObject()) {
            return null;
        }

        AudioFeature audioFeature = new AudioFeature();
        audioFeature.setDanceability(audioFeatureJson.getDouble("danceability"));
        audioFeature.setEnergy(audioFeatureJson.getDouble("energy"));
        audioFeature.setKey(audioFeatureJson.getInt("key"));
        audioFeature.setLoudness(audioFeatureJson.getDouble("loudness"));
        audioFeature.setMode(audioFeatureJson.getInt("mode"));
        audioFeature.setSpeechiness(audioFeatureJson.getDouble("speechiness"));
        audioFeature.setAcousticness(audioFeatureJson.getDouble("acousticness"));
        audioFeature.setInstrumentalness(audioFeatureJson.getDouble("instrumentalness"));
        audioFeature.setLiveness(audioFeatureJson.getDouble("liveness"));
        audioFeature.setValence(audioFeatureJson.getDouble("valence"));
        audioFeature.setTempo(audioFeatureJson.getDouble("tempo"));
        audioFeature.setType(audioFeatureJson.getString("type"));
        audioFeature.setId(audioFeatureJson.getString("id"));
        audioFeature.setUri(audioFeatureJson.getString("uri"));
        audioFeature.setTrackHref(audioFeatureJson.getString("track_href"));
        audioFeature.setAnalysisUrl(audioFeatureJson.getString("analysis_url"));
        audioFeature.setDurationMs(audioFeatureJson.getInt("duration_ms"));
        audioFeature.setTimeSignature(audioFeatureJson.getInt("time_signature"));


        return audioFeature;
    }
    private static List<AudioFeature> createMultipleAudioFeature(JSONObject audioFeatureJsonList) {
        if (audioFeatureJsonList == null || audioFeatureJsonList.isNullObject()) {
            return null;
        }
        List<AudioFeature> returnedAudioFetureList = new ArrayList<>();
        JSONArray audioFeatureList = audioFeatureJsonList.getJSONArray("audio_features");
        for (int i = 0; i < audioFeatureList.size(); i++) {
            returnedAudioFetureList.add(createAudioFeature(audioFeatureList.getJSONObject(i)));
        }

        return returnedAudioFetureList;
    }

}
