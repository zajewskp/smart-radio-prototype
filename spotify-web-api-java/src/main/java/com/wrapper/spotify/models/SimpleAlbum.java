package com.wrapper.spotify.models;

import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Transient;

import java.util.List;

public class SimpleAlbum {
  @Transient
  private AlbumType albumType;
  @Transient
  private ExternalUrls externalUrls;
  private String href;
  private String id;
  @Transient
  private List<Image> images;
  @Property
  private String name;
  @Transient
  private SpotifyEntityType type = SpotifyEntityType.ALBUM;
  private String uri;
  @Transient
  private List<String> availableMarkets;

  public List<String> getAvailableMarkets() {
    return availableMarkets;
  }

  public void setAvailableMarkets(List<String> availableMarkets) {
    this.availableMarkets = availableMarkets;
  }

  public AlbumType getAlbumType() {
    return albumType;
  }

  public void setAlbumType(AlbumType albumType) {
    this.albumType = albumType;
  }

  public ExternalUrls getExternalUrls() {
    return externalUrls;
  }

  public void setExternalUrls(ExternalUrls externalUrls) {
    this.externalUrls = externalUrls;
  }

  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<Image> getImages() {
    return images;
  }

  public void setImages(List<Image> images) {
    this.images = images;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public SpotifyEntityType getType() {
    return type;
  }

  public void setType(SpotifyEntityType type) {
    this.type = type;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }
}
