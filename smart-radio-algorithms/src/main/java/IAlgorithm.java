import java.io.File;

public interface IAlgorithm {
    public void generatePredictions(File trainData, File testData)  throws Exception;
}
