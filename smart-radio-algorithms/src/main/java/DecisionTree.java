import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.output.prediction.XML;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.JSONLoader;
import weka.filters.unsupervised.attribute.Remove;

import java.io.BufferedReader;
import java.io.File;

public class DecisionTree implements IAlgorithm {
    @Override
    public void generatePredictions(File trainData, File testData) throws Exception {
        JSONLoader train = new JSONLoader();
        JSONLoader test = new JSONLoader();
        train.setSource(trainData);
        test.setSource(testData);
        Instances trainInstance = train.getDataSet();
        Instances testInstance = test.getDataSet();
        trainInstance.setClassIndex(trainInstance.numAttributes() - 1);
        testInstance.setClassIndex(testInstance.numAttributes() - 1);
        Remove rm = new Remove();
        rm.setAttributeIndicesArray(new int[]{0, 1});
        J48 j48 = new J48();
        j48.setMinNumObj(10);
        FilteredClassifier classifier = new FilteredClassifier();
        classifier.setFilter(rm);
        classifier.setClassifier(j48);
        classifier.buildClassifier(trainInstance);
        for (int i = 0; i < testInstance.numInstances(); i++){
            double prediction = classifier.classifyInstance(testInstance.instance(i));
            System.out.print(testInstance.instance(i).stringValue(0));
            System.out.print(" - " + testInstance.instance(i).stringValue(1));
            System.out.println(", predicted: " + testInstance.classAttribute().value((int) prediction));
        }
    }
}
