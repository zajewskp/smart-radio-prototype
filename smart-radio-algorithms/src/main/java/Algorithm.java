import java.io.BufferedReader;
import java.io.File;

public class Algorithm {
    private IAlgorithm algorithm = null;

    public Algorithm(String algorithmName){
        if(algorithmName.equalsIgnoreCase("decisionTree")){
            algorithm = new DecisionTree();
        }
        else if (algorithmName.equalsIgnoreCase("naiveBayes")){
            algorithm = new NaiveBayesGenerator();
        }
        else if (algorithmName.equalsIgnoreCase("neuralNetwork")){
            algorithm = new NeuralNetwork();
        }
    }

    public void generatePredictions(File train, File test) throws Exception {
        algorithm.generatePredictions(train, test);
    }

}
