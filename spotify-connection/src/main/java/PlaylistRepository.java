import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import models.PlaylistFullInfo;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.net.UnknownHostException;
import java.util.ArrayList;

public class PlaylistRepository {

    private MongoClient mongoClient;
    private DB db;
    private final Morphia morphia = new Morphia();
    private final Datastore datastore;
    private ArrayList<String> labels;

    public PlaylistRepository(String configName) throws UnknownHostException {
        this.mongoClient = new MongoClient("127.0.0.1",27017);
        this.db = mongoClient.getDB("smart_radio");
        System.out.println("Connected to Database");
        morphia.mapPackage("models.PlaylistFullInfo");
        morphia.mapPackage("com.wrapper.spotify.models");
        this.datastore = morphia.createDatastore(mongoClient, "smart_radio");
    }

    public boolean playlistExists(String id){
        BasicDBObject query = new BasicDBObject("_id", id);
        DBCursor cursor = this.db.getCollection("PlaylistFullInfo").find(query);
        if(cursor.count() > 0){
            return true;
        }
        return false;
    }

    public void add(PlaylistFullInfo playlist) {
        this.datastore.save(playlist.getTracks());
        this.datastore.save(playlist);
    }

    public PlaylistFullInfo getPlaylistFullInfo(String playlistId){
        return this.datastore.find(PlaylistFullInfo.class).field("_id").equal(playlistId).get();
    }


}