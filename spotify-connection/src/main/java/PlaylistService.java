import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.wrapper.spotify.exceptions.WebApiException;
import com.wrapper.spotify.models.AudioFeature;
import com.wrapper.spotify.models.Playlist;
import com.wrapper.spotify.models.PlaylistTrack;
import models.PlaylistFullInfo;
import models.TrackFullInfo;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.security.auth.login.Configuration;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class PlaylistService implements IPlaylistService {

    private SpotifyApi spotifyApi;
    private PlaylistRepository playlistRepository;
    private WekaGenerator wekaGenerator;
    private String configName;
    private Config conf;


    public void init(String clientId, String clientSecret, String configName) throws IOException, WebApiException, InterruptedException {
        this.spotifyApi = new SpotifyApi(clientId,clientSecret);
        this.playlistRepository = new PlaylistRepository(configName);
        this.wekaGenerator = new WekaGenerator(configName);
        this.configName = configName;
        this.conf = ConfigFactory.load();
        System.out.println("PlaylistService initialized");
    }

    public PlaylistFullInfo downloadPlaylist(String userId, String playlistId) throws IOException, WebApiException, InterruptedException {
            List<PlaylistTrack> playlistTracks = this.spotifyApi.requestPlaylist(userId, playlistId);
            List<AudioFeature> multipleAudioFeatures = new ArrayList<AudioFeature>();
            List<TrackFullInfo> trackFullInfoList = new ArrayList<TrackFullInfo>();
            Playlist playlistInfo = this.spotifyApi.requestPlaylistInfo(userId, playlistId);
            // temporary list of Ids
            List<String> trackIds = new ArrayList<String>();
            for (int i = 0; i < playlistTracks.size(); i++) {
                if (i == 0) {
                    // add first id
                    trackIds.add(playlistTracks.get(i).getTrack().getId());
                } else if ((i % 50) == 0) {
                    // every 50 id's make request for audio features
                    trackIds.add(playlistTracks.get(i).getTrack().getId());
                    multipleAudioFeatures.addAll(this.spotifyApi.requestAudioFeatures(trackIds));
                    trackIds.clear();
                } else {
                    trackIds.add(playlistTracks.get(i).getTrack().getId());
                }
            }
            // request with the rest of id's
            multipleAudioFeatures.addAll(this.spotifyApi.requestAudioFeatures(trackIds));
            trackIds.clear();
            if (multipleAudioFeatures.size() == playlistTracks.size()) {
                for (int i = 0; i < playlistTracks.size(); i++) {
                    trackFullInfoList.add(
                            new TrackFullInfo(
                                    playlistTracks.get(i).getTrack(),
                                    multipleAudioFeatures.get(i)
                            )
                    );
                }
            } else {
                System.err.println("Something went wrong. Unmatched sizes of tracks and audio_features lists.");
                return null;
            }
            return new PlaylistFullInfo(playlistInfo, trackFullInfoList);
    }

    public boolean playlistExists(String playlistId){
        return playlistRepository.playlistExists(playlistId);
    }
    public void savePlaylistToDatabase(PlaylistFullInfo playlist) throws IOException, InterruptedException {
        if(playlist != null){
            playlistRepository.add(playlist);
        }
    }
    public void convertDataForWeka(List<String> playlistIds, Boolean learning) throws FileNotFoundException {

        JsonArrayBuilder dataArray = Json.createArrayBuilder();
        for(String playlistId: playlistIds) {
            dataArray = wekaGenerator.dataArrayForWekaBuilder(dataArray, playlistRepository.getPlaylistFullInfo(playlistId), learning);
        }

        PrintStream ps = new PrintStream(conf.getString(configName + ".filename"));
        ps.println(wekaGenerator.wekaSchemaBuilder(dataArray));
        System.out.println("regex for non-ascii values: [^\\x00-\\x7F]+\n");
    }

}