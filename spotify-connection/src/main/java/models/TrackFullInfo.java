package models;

import com.wrapper.spotify.models.AudioFeature;
import com.wrapper.spotify.models.Track;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import java.io.Serializable;

@Entity
public class TrackFullInfo implements Serializable {
    @Id
    private String id;
    @Embedded
    private Track track;
    @Embedded
    private AudioFeature audioFeature;

    public TrackFullInfo(){
        audioFeature = null;
        track = null;
    }
    public TrackFullInfo (Track track, AudioFeature audioFeature){
        this.track = track;
        this.audioFeature = audioFeature;
        this.id = track.getId();
    }

    public Track getTrack() {
        return track;
    }
    public AudioFeature getAudioFeature() {
        return audioFeature;
    }

}
