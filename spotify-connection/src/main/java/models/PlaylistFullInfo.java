package models;

import com.wrapper.spotify.models.Playlist;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import java.util.List;
@Entity
public class PlaylistFullInfo {
    @Id
    private String id;
    @Embedded
    private Playlist info;
    @Reference
    private List<TrackFullInfo> tracks;

    public PlaylistFullInfo(){
        this.info = null;
        this.tracks = null;
        this.id = null;
    }
    public PlaylistFullInfo(Playlist info, List<TrackFullInfo> tracks){
        this.info = info;
        this.tracks = tracks;
        this.id = info.getId();
    }

    public List<TrackFullInfo> getTracks() {
        return tracks;
    }

    public Playlist getInfo() {
        return info;
    }
}
