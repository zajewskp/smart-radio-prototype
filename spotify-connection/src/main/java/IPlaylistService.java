import com.wrapper.spotify.exceptions.WebApiException;
import models.PlaylistFullInfo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface IPlaylistService {
    void init(String clientId, String clientSecret, String configName) throws IOException, WebApiException, InterruptedException;

    PlaylistFullInfo downloadPlaylist(String userId, String playlistId) throws IOException, WebApiException, InterruptedException;

    boolean playlistExists(String playlistId);

    void savePlaylistToDatabase(PlaylistFullInfo playlist) throws IOException, InterruptedException;

    void convertDataForWeka(List<String> playlistIds, Boolean learning) throws FileNotFoundException;
}
