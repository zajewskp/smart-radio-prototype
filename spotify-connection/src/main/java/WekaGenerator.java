import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import models.PlaylistFullInfo;
import models.TrackFullInfo;

import javax.json.*;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class WekaGenerator {

    private String configName;
    private Config conf;

    public WekaGenerator(String configName){
        this.configName = configName;
        this.conf = ConfigFactory.load();
    }
    private JsonObjectBuilder attribute(String name, String type, boolean isClass, double weight){
        JsonObjectBuilder attribute = Json.createObjectBuilder()
                .add("name", name)
                .add("type", type)
                .add("class",isClass)
                .add("weight", weight);
        if(isClass){
            JsonArrayBuilder array = Json.createArrayBuilder();
            List<String> configLabels = conf.getStringList(configName + ".labels");
            for(String label: configLabels){
                array.add(label);
            }
            attribute.add("labels", array);
        }
        return attribute;
    }

    private JsonObjectBuilder data(JsonArray values) {
        return Json.createObjectBuilder()
                .add("sparse", false)
                .add("weight", 1.0)
                .add("values",values);
    }

    private JsonArray generateTrackValues(TrackFullInfo track, String condition){
        List<String> configAudioFeatures = conf.getStringList(configName + ".audioFeatures");
        JsonArrayBuilder audioFeatures = Json.createArrayBuilder();
        for(String feature: configAudioFeatures) {
            switch (feature) {
                case "artist":
                audioFeatures.add(track.getTrack().getArtists().get(0).getName());
                break;
                case "title":
                audioFeatures.add(track.getTrack().getName());
                break;
                case "danceability":
                audioFeatures.add(Double.toString(track.getAudioFeature().getDanceability()));
                break;
                case "energy":
                audioFeatures.add(Double.toString(track.getAudioFeature().getEnergy()));
                break;
                case "key":
                audioFeatures.add(Double.toString(track.getAudioFeature().getKey()));
                break;
                case "loudness":
                audioFeatures.add(Double.toString(track.getAudioFeature().getLoudness()));
                break;
                case "mode":
                audioFeatures.add(Double.toString(track.getAudioFeature().getMode()));
                break;
                case "speechiness":
                audioFeatures.add(Double.toString(track.getAudioFeature().getSpeechiness()));
                break;
                case "acousticness":
                audioFeatures.add(Double.toString(track.getAudioFeature().getAcousticness()));
                break;
                case "instrumentalness":
                audioFeatures.add(Double.toString(track.getAudioFeature().getInstrumentalness()));
                break;
                case "liveness":
                audioFeatures.add(Double.toString(track.getAudioFeature().getLiveness()));
                break;
                case "valence":
                audioFeatures.add(Double.toString(track.getAudioFeature().getValence()));
                break;
                case "tempo":
                audioFeatures.add(Double.toString(track.getAudioFeature().getTempo()));
                break;
            }
        }
        audioFeatures.add(condition);
        return audioFeatures.build();
    }

    private JsonObjectBuilder wekaHeaderBuilder(){
        List<String> configAudioFeatures = conf.getStringList(configName + ".audioFeatures");
        JsonArrayBuilder attributes = Json.createArrayBuilder();
        for(String feature: configAudioFeatures) {
            switch (feature) {
                case "artist":
                    attributes.add(attribute("artist","string",false,0.0));
                    break;
                case "title":
                    attributes.add(attribute("title","string",false,0.0));
                    break;
                case "danceability":
                    attributes.add(attribute("danceability","numeric",false,1.0));
                    break;
                case "energy":
                    attributes.add(attribute("energy","numeric",false,1.0));
                    break;
                case "key":
                    attributes.add(attribute("key","numeric",false,1.0));
                    break;
                case "loudness":
                    attributes.add(attribute("loudness","numeric",false,1.0));
                    break;
                case "mode":
                    attributes.add(attribute("mode","numeric",false,1.0));
                    break;
                case "speechiness":
                    attributes.add(attribute("speechiness","numeric",false,1.0));
                    break;
                case "acousticness":
                    attributes.add(attribute("acousticness","numeric",false,1.0));
                    break;
                case "instrumentalness":
                    attributes.add(attribute("instrumentalness","numeric",false,1.0));
                    break;
                case "liveness":
                    attributes.add(attribute("liveness","numeric",false,1.0));
                    break;
                case "valence":
                    attributes.add(attribute("valence","numeric",false,1.0));
                    break;
                case "tempo":
                    attributes.add(attribute("tempo","numeric",false,1.0));
                    break;
            }
        }
        attributes.add(attribute("condition","nominal",true,1.0));

        JsonObjectBuilder  header = Json.createObjectBuilder()
                .add("relation", "smart-radio")
                .add("attributes",attributes);

        return header;
    }

    public String wekaSchemaBuilder(JsonArrayBuilder  dataArray){
        Map<String, Object> config = new HashMap<String, Object>();
        JsonBuilderFactory factory = Json.createBuilderFactory(config);
        config.put("javax.json.stream.JsonGenerator.prettyPrinting", Boolean.valueOf(true));
        String json = factory.createObjectBuilder()
                .add("header",wekaHeaderBuilder())
                .add("data", dataArray).build().toString();

        return json;
    }
    public JsonArrayBuilder dataArrayForWekaBuilder(JsonArrayBuilder dataArray, PlaylistFullInfo playlistFullInfo, Boolean learning) throws FileNotFoundException {
        String condition = "?";

        if(learning){
            condition =  playlistFullInfo.getInfo().getName();
        }
        Iterator<TrackFullInfo> iterator = playlistFullInfo.getTracks().iterator();
        while (iterator.hasNext()) {
            dataArray.add(data(generateTrackValues(iterator.next(),condition)));
        }
        return dataArray;

    }
}
