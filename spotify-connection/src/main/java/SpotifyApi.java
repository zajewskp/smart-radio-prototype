import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.SettableFuture;
import com.wrapper.spotify.Api;
import com.wrapper.spotify.exceptions.WebApiException;
import com.wrapper.spotify.methods.MultipleAudioFeatureRequest;
import com.wrapper.spotify.methods.PlaylistRequest;
import com.wrapper.spotify.methods.PlaylistTracksRequest;
import com.wrapper.spotify.methods.authentication.ClientCredentialsGrantRequest;
import com.wrapper.spotify.models.*;
import models.TrackFullInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SpotifyApi {
    private final String clientId;
    private final String clientSecret;
    private Date tokenExpirationDate;
    private Api accessApi;


    public SpotifyApi(String clientId, String clientSecret) throws IOException, WebApiException {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        setAccessApi();
    }

    private void setAccessApi() throws IOException, WebApiException {
        Api api = Api.builder()
                .clientId(this.clientId)
                .clientSecret(this.clientSecret)
                .build();
        ClientCredentialsGrantRequest request = api.clientCredentialsGrant().build();
        ClientCredentials response = request.get();
        this.tokenExpirationDate = new Date(System.currentTimeMillis() + response.getExpiresIn());
        String accessToken = response.getAccessToken();
        this.accessApi = new Api.Builder().accessToken(accessToken).build();
    }

    private void checkExpirationToken() throws IOException, WebApiException {
        Date currentDate = new Date();
        if(currentDate.compareTo(this.tokenExpirationDate) > 0) {
            return;
        } else {
            setAccessApi();
        }
    }

    public List<PlaylistTrack> requestPlaylist(String userId, String playlistId) throws IOException, WebApiException {
        checkExpirationToken();
        int offset = 100;
        List<PlaylistTrack> fullPlaylist;
        PlaylistTracksRequest request = this.accessApi.getPlaylistTracks(userId, playlistId).build();
        Page<PlaylistTrack> playlistPage = request.get();
        fullPlaylist = playlistPage.getItems();
        while(playlistPage.getNext() != null){
            request = this.accessApi.getPlaylistTracks(userId, playlistId).offset(offset).build();
            playlistPage = request.get();
            fullPlaylist.addAll(playlistPage.getItems());
            offset = offset + playlistPage.getItems().size();
        }
        return  fullPlaylist;
    }

    public Playlist requestPlaylistInfo(String userId, String playlistId) throws IOException, WebApiException {
        String fields = "name,description,owner,public,id,uri,href";
        checkExpirationToken();
        PlaylistRequest request = this.accessApi.getPlaylist(userId,playlistId,fields).build();
        return request.get();
    }

    public List<AudioFeature> requestAudioFeatures(List<String> ids) throws IOException, WebApiException {
        checkExpirationToken();
        int offset = 100;
        List<AudioFeature> MultipleAudioFeatures;
        MultipleAudioFeatureRequest request = this.accessApi.getAudioFeature(ids).build();
        MultipleAudioFeatures = request.get();
        return MultipleAudioFeatures;

    }

}