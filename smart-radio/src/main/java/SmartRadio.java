import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.wrapper.spotify.exceptions.WebApiException;
import models.PlaylistFullInfo;

import java.io.*;
import java.util.List;

public class SmartRadio {

    private static boolean loading = false;
    private static boolean update = false;
    private static boolean skip = false;

    public static void main(String[] args) {
        try {
            if (args[0].equals("generateData")) {
                String configName = args[1];
                Config conf = ConfigFactory.load();
                String clientId = conf.getString(configName + ".clientId");
                String clientSecret = conf.getString(configName + ".clientSecret");
                String userId = conf.getString(configName + ".userId");
                List<String> playlistsIds = conf.getStringList(configName + ".playlistsIds");
                Boolean learning = conf.getBoolean(configName + ".learning");

                IPlaylistService playlistService = new PlaylistService();

                try {
                    playlistService.init(clientId, clientSecret, configName);
                    for (String playlistId : playlistsIds) {
                        downloadAndSavePlaylist(playlistService, userId, playlistId);
                    }
                    playlistService.convertDataForWeka(playlistsIds, learning);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (WebApiException e1) {
                    e1.printStackTrace();
                }
            } else if (args[0].equals("run")) {
                String algorithmName = args[1];
                File train = new File(args[2]);
                File test = new File(args[3]);
                Algorithm algorithm = new Algorithm(algorithmName);
                algorithm.generatePredictions(train,test);
            } else {
                System.out.println("Wrong command, try again.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void downloadAndSavePlaylist(IPlaylistService playlistService, String userId, String playlistId) throws IOException, InterruptedException, WebApiException {
        String command = null;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        if (skip){
            return;
        }
        if (playlistService.playlistExists(playlistId) && !update) {
            while (true) {
                System.out.println("Playlist " + playlistId + " already exists in the database. Update the playlist?");
                command = bufferedReader.readLine();
                if (command.equals("yes")) {
                    loading = true;
                    loading("Downloading Playlist...");
                    PlaylistFullInfo playlist = playlistService.downloadPlaylist(userId,playlistId);
                    playlistService.savePlaylistToDatabase(playlist);
                    loading = false;
                    break;
                }
                if (command.equals("no")) {
                    System.out.println("Playlist unsaved");
                    break;
                }
                if (command.equals("yes all")){
                    update = true;
                    loading = true;
                    loading("Downloading Playlist...");
                    PlaylistFullInfo playlist = playlistService.downloadPlaylist(userId,playlistId);
                    playlistService.savePlaylistToDatabase(playlist);
                    loading = false;
                    break;
                }
                if (command.equals("no all")){
                    skip = true;
                    break;
                }
            }
        } else {
            loading = true;
            loading("Downloading Playlist...");
            PlaylistFullInfo playlist = playlistService.downloadPlaylist(userId,playlistId);
            playlistService.savePlaylistToDatabase(playlist);
            loading = false;
        }

    }


    private static synchronized void loading(String msg) throws IOException, InterruptedException {
        System.out.println(msg);

        Thread th = new Thread() {
            @Override
            public void run() {
                try {
                    System.out.write("\r|".getBytes());
                    while(loading) {
                        System.out.write("-".getBytes());
                        sleep(500);
                    }
                    System.out.write("| Done \r\n".getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        th.start();
    }
}
